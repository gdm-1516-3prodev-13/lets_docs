# PRODUCTIEDOSSIER

| Groep      | Wie?       | Project   | Vak                         | 
| :--------- | :--------- | :-------- | :-------------------------- |
| 3PRODEV-13 | Denys Nick | Deal      | Web Design & Development IV |


## Briefing
### Probleemstelling
Hoe kan een softwaresysteem ruilhandel stimuleren?
### Opdracht
Ontwerp en ontwikkel individueel een databasegebaseerd softwaresysteem gemaakt met de technologieën die tijdens de colleges aan bod komen, en bestaat uit:

* Frontoffice
* Backoffice

### Omschrijving
> LETS staat voor "Local Exchange and Trading System". Vrij vertalen we dit wel eens als "lokaal uitwisselings systeem". Het ruilsysteem in deze vorm ontstond in een plaats in Canada waar op korte termijn de werkverschaffing en dus rechtstreeks de inkomsten van mensen wegtrok. Uit noodzaak gingen zij weer ruilen, maar dan wel op een eigentijdse manier. Nadien heeft het idee zich snel verspreid over de hele wereld.

## Analyse

### Basics
Ons digitaal LETS-systeem heeft volgende basiselementen nodig:

1. Een groep mensen met een vraag- en aanbodlijst
2. Een eenheden-stelsel, gebaseerd op tijd
3. Een gids of krant waarin de leden kenbaar maken wat ze kunnen aanbieden of zelf nodig hebben
4. Een manier om diensten te waarderen, m.a.w. standen bijhouden voor iedereen

#### Naamgeving
_Woordenwolk_

* LETS
* Local
* Exchange
* Trading
* System
* Timebank
* Bartering
* Swap
* Transfer
* **Deal**
* Value
* Offer

### Mogelijke problemen
Volgende problemen kunnen zich voordoen en moeten geadresseerd worden in ons systeem

* Leden kunnen ver onder de nul gaan met hun krediet, zonder weinig terug te doen.
* Verlegen mensen zullen niet altijd even uitreiken naar vreemden op het internet.
* LETS-groepen moeten steeds vrijblijvend draaien! Leden met hogere kredieten kunnen niet zomaar eisen stellen.
* Een LETS-systeem kan traag verlopen door gebrek aan initiatief.
* Diensten binnen LETS zijn niet professioneel en dus bij voorbaat op individueel vertrouwen.

### Inspiratiebronnen
* [http://hourworld.org/_TimeAndTalents.htm](http://hourworld.org/_TimeAndTalents.htm)
* [http://www.time-bank.info/](http://www.time-bank.info/)
* [http://www.letslinkuk.net/](http://www.letslinkuk.net/)
* [http://www.bristollets.org.uk/members/index.php](http://www.bristollets.org.uk/members/index.php)
* [https://democw3.timebanks.org/](https://democw3.timebanks.org/)

## Technische specificaties
>Wat zijn de technische eisen aan het product? Waaraan moet het product voldoen?

### Technische aspecten
* De applicatie is volledig responsive
* Er is een eigen ontwikkeld en grafisch verantwoord ontwerp toegepast voor zowel de backoffice als frontoffice.

### Rollen
* _Lid_
	* kan aanbiedingen posten
	* kan op aanbiedingen reageren
* _Groepscoördinator_
	* kan leden uit de eigen groep beheren
	* kan alle inhoud uit de groep bekijken
* _Administrator_
	* kan alle inhoud beheren

	
### Structuur
Alle handelingen die binnen de applicatie worden aangeboden, zullen worden gecategoriseerd  bvb. huisklusjes, koken of onderwijs. Gebruikers kunnen daarna binnen die categorieën specifieke aanbiedingen terugvinden.

#### Informatie per pagina
_Algemeen_

* Snel overzicht eigen statistieken
* Glasheldere en duidelijke navigatie
	* Home
	* *My profile*
	* *My offers*
	* *My requests*
	* *My groups*
	* Leaderboard
	* Help
	* Contact
	* Login/Register

_Home_

* Wat is dit? Waar ben ik?
* Mogelijkheid tot registeren en inloggen.
* Snel weergeven van uw meest recente:
	* offers
	* requests
	* groepen

_My profile_

* Weergeven en bewerken van persoonlijke gegevens
* Overzicht van eigen
	* groepen
	* offers
	* requests

_My offers_

* Browsen doorheen uw
	* actieve offers
	* voltooide offers

_My requests_

* Browsen doorheen uw
	* actieve requests
	* voltooide requests

_My groups_

* Browsen doorheen uw
	* ingeschreven groepen
	* niet-ingeschreven groepen	

### Krediet
Het kredietsysteem zal bijna volledig op tijd gebaseerd zijn, meerbepaald minuten. Voor elke minuut werk die verricht is, zal er 1 krediet worden toegekend aan de werknemer.

Elke aanbieding zal ook in 3 categorieën worden ingedeeld nl. licht, gemiddeld en zwaar werk. Na elk voltooide aanbieding zullen er extra kredieten toegekend worden. De aanbiedingen in de zware categorie is moeilijker te voltooien dan de lichte en is dus ook "duurder". De extra kredieten worden berekend per percentage van het totaal:

* Licht werk: 5%
* Medium werk: 10%
* Zwaar werk: 15%

## Functionele specificaties
>Welk gebruiksdoel of prestatie levert het product? Wat is het gewenst gedrag van het product?

Volgende basisfunctionaliteiten zijn aanwezig:

* Inlog en registratiesysteem
* Het systeem kan groepen bevatten
* Leden kunnen groepen starten
* Leden kunnen zich aansluiten bij groepen
* Leden kunnen groepen verlaten
* Leden kunnen aanbiedingen posten / bewerken / verwijderen
* Leden kunnen op aanbiedingen reageren
* De aanbieder kan nadien op de dienst reageren: geslaagd of niet geslaagd
* Het systeem kent automatisch punten toe aan de dienstverlener indien geslaagd
* Het systeem trekt automatisch punten af bij de aanbieder indien de opdracht geslaagd is

Extra functionaliteiten zijn als volgt:

* Aanbieders kunnen bij de voltooide diensten een commentaar nalaten
* Leden hebben hun eigen profielpagina
* Leden kunnen op die profielpagina een geschiedenis bekijken van vorige opdrachten

Toekomstige functionaliteiten kunnen zijn:

* Leden kunnen elkaar privéberichten sturen.
* Aanbieders kunnen de voltooide diensten een score geven.
* Leden kunnen op profielpagina's goede/slechte referenties schrijven.

## Persona’s
> ![Jan](images/personas/jan.jpg)
> 
* Jan Alleman, 45 jaar, Kortrijk
* Alleenstaand
* Werkt bij KBC
* Technische kennis: gemiddeld

> Jan werkt lange dagen in het KBC-kantoor in z'n thuisstad Kortrijk. Hij heeft daarom niet veel tijd om huisklusjes te doen. Er waren onlangs veel vergaderingen waardoor Jan weinig tijd had. Gevolg: het gras moet dringend gemaaid worden. Hij schrijft zich in op de lokale LETS-groep van Kortrijk. Hij zoekt dus iemand die dat wil doen voor hem. In ruil biedt hij tegelijk zijn expertise in financiën aan zodat hij anderen daarmee kan helpen via internet.

---

>![Sofie](images/personas/sofie.jpg)
>
* Sofie Brac, 55 jaar, Gent
* Gehuwd
* Kuisvrouw, gestopt met werken ondertussen
* Technische kennis: laag

> Sofie is een vroegere kuisvrouw die heeft moeten stoppen met werken door stressgerelateerde aandoeningen. Doordat ze overdag niet alleen thuis wil zitten niks doen, schrijft ze zich in voor de LETS-groep van Gent. Ze biedt aan om te kuisen en zoekt terwijl iemand op wat bij te klussen in haar huis, aangezien haar man een kluts is met twee linkerhanden.

## Sitemap
### Frontoffice
![Frontoffice]
(images/sitemaps/frontofficev2.png)

### Backoffice
![Backoffice]
(images/sitemaps/backofficev2.png)

## Branding
### Logo
![Logo](images/branding/logo.png)
## Wireframes
###Desktop (HD)
![Desktop HD](images/wireframes/desktop_hd.png)

###Desktop
![Desktop](images/wireframes/desktop.png)

###Tablet
![Tablet](images/wireframes/tablet_portrait.png)

###Mobile
![Mobile](images/wireframes/mobile.png)

## Style Tiles
(*externe links)

[Style Tile 1](http://school.nickdenys.com/wdad4/style_guides/1/)

[Style Tile 2](http://school.nickdenys.com/wdad4/style_guides/2/)

[Style Tile 3](http://school.nickdenys.com/wdad4/style_guides/3/)

Er werd uiteindelijk gekozen voor Style Tile 1.

## Visual Design
###Desktop (HD)
![Desktop HD](images/visual_design/desktop_hd.jpg)

###Desktop
![Desktop](images/visual_design/desktop.jpg)

###Tablet
![Tablet](images/visual_design/tablet_portrait.jpg)

###Mobile
![Mobile](images/visual_design/mobile.jpg)


## Screenshots
### Application
#### Home (Logged in)
![Home](images/screenshots/home-1280.png)

#### My Requests
![My requests](images/screenshots/requests-1280.png)

#### Request (detail)
![Request](images/screenshots/request-1280.png)

#### Group (detail)
![Group](images/screenshots/group-1280.png)

#### Profile
![Profile](images/screenshots/profile-1280.png)

#### Backoffice
![Backoffice](images/screenshots/backoffice-1280.png)

### Code

#### ViewModel
![ViewModel](images/screenshots/code-viewmodel.png)

#### Controller
![ViewModel](images/screenshots/code-controller.png)

#### View
![ViewModel](images/screenshots/code-view.png)
