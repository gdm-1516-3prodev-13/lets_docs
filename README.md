# Deal

![Deal](images/branding/logo.png)

## Omschrijving

Deal is een easy-to-use LETS-systeem die gebaseerd is op indirecte ruilhandel waarbij de leden betalen met een virtueel krediet.

## Documenten

* [Productiedossier](dossier.md)
* [Acedemische Poster](poster.pdf)
* [Timesheet](timesheet.xlsx)
* [Repository](https://bitbucket.org/gdm-1516-3prodev-13/lets_app)

## Screenshots

![Deal](screenshot_1280.png)

## Auteur

**Nick Denys**

* <http://twitter.com/nickdenys>
* <http://github.com/nickdenys>
* <http://bitbucket.org/nickdenys>
* <hello@nickdenys.com>

## Arteveldehogeschool

- <http://www.arteveldehogeschool.be>
- <http://www.arteveldehogeschool.be/ects>
- <http://www.arteveldehogeschool.be/bachelor-de-grafische-en-digitale-media>
- <http://twitter.com/bachelorGDM>
- <https://www.facebook.com/BachelorGrafischeEnDigitaleMedia?fref=ts>

## Copyright and license

Code en documentatie copyright 2003-2016 Arteveldehogeschool | Opleiding Grafische en digitale media | drdynscript. Cursusmateriaal, zoals code, documenten, ... uitgebracht onder de Apache License licentie.

*In opdracht van Web Design & Development IV, Arteveldehogeschool, 2015-2016*
